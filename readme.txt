1. Right-Click on Garry's Mod in your Steam library.
2. Click on "Properties..."
3. Goto the "LOCAL FILES" tab & click on "BROWSE LOCAL FILES..."
4. Goto the "garrysmod" folder, then "gamemodes".
5. Copy paste "nutscript" & "starwarsrp" to the "gamemodes" folder you just opened.

Nutscript: https://github.com/rebel1324/NutScript/tree/1.1
Half-Life 2 Base Schema: https://github.com/Chessnut/hl2rp
